+++ 
date = "2020-01-01"
title = "Silvester 2019"
slug = "silvester-2019" 
tags = ["silvester", "fireworks"]
categories = ["gallery","silvester","celebrations"]
+++

| [Sparklers in Franconia](https://www.instagram.com/p/B608_fwoVOr/) |
|---------------------------------------------------------------------|
| ![Alt text](/images/galleries/silvester2019/1.jpg "a title") |
| ![Alt text](/images/galleries/silvester2019/2.jpg "a title") |
